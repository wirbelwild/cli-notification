[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/cli-notification)](http://www.php.net)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/ca78d1a37af344f6878dd0f26e4f466b)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/cli-notification&amp;utm_campaign=Badge_Grade)
[![Total Downloads](https://poser.pugx.org/bitandblack/cli-notification/downloads)](https://packagist.org/packages/bitandblack/cli-notification)
[![License](https://poser.pugx.org/bitandblack/cli-notification/license)](https://packagist.org/packages/bitandblack/cli-notification)

# CLI Notification

Notification sounds and popups from the CLI.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/cli-notification). Add it to your project by running `$ composer require bitandblack/cli-notification`.

## Usage 

### Sound

Whenever you want to play a sound, call the `Sound::play()` method. It needs one parameter — the sound you want to play. You can use the [SoundFileEnum](./src/SoundFile/SoundFileEnum.php) that holds some [basic sounds](./sounds) or provide your own class holding custom sounds by implementing the [SoundFileInterface](./src/SoundFile/SoundFileInterface.php).

The `play()` method is static, so you don't need to initialize the class.

````php
<?php

use BitAndBlack\CLINotification\Sound;
use BitAndBlack\CLINotification\SoundFile\SoundFileEnum;

Sound::play(
    SoundFileEnum::JOB_DONE()
);
````

### Popup

Whenever you want to show a popup, call the `Popup::show()` method. You can set the title, the message and an image there.

The `show()` method is static, so you don't need to initialize the class.

````php
<?php

use BitAndBlack\CLINotification\Popup;

Popup::show(
    'Title', 
    'Message', 
    '/path/to/image.jpg'
);
````

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).