<?php

/**
 * CLI Notification.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\CLINotification;

use BitAndBlack\CLINotification\Exception\SoundFileMissingException;
use BitAndBlack\CLINotification\SoundFile\SoundFileInterface;

class Sound
{
    /**
     * @var array<string, string>
     */
    private static array $commands = [
        'afplay' => '"{{file}}"',
        'ffplay' => '"{{file}}" -nodisp -autoexit',
        'powershell' => '-c (New-Object Media.SoundPlayer "{{file}}").PlaySync();',
    ];

    /**
     * @param SoundFileInterface $soundFileProvider
     * @return bool
     * @throws SoundFileMissingException
     */
    public static function play(SoundFileInterface $soundFileProvider): bool
    {
        $soundFile = $soundFileProvider->getValue();

        if (!file_exists($soundFile)) {
            throw new SoundFileMissingException($soundFile);
        }

        $command = self::getAvailableCommand();

        if (null === $command) {
            return false;
        }

        $commandEncoded = str_replace('{{file}}', $soundFile, self::$commands[$command]);

        exec($command . ' ' . $commandEncoded);

        return true;
    }

    /**
     * Looks for the available command and returns its name.
     *
     * @return string|null
     */
    private static function getAvailableCommand(): ?string
    {
        $commands = array_keys(self::$commands);
        
        foreach ($commands as $command) {
            if (self::isCommandAvailable($command)) {
                return $command;
            }
        }
        
        return null;
    }

    /**
     * Returns if a command is available.
     *
     * @param string $name
     * @return bool
     */
    private static function isCommandAvailable(string $name): bool
    {
        exec($name . ' -v > /dev/null 2>&1 & echo $!', $version, $exitCode);
        return !empty($version) && 0 === $exitCode;
    }
}
