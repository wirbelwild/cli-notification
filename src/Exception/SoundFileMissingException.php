<?php

/**
 * CLI Notification.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\CLINotification\Exception;

use BitAndBlack\CLINotification\Exception;
use Throwable;

class SoundFileMissingException extends Exception
{
    public function __construct(string $soundFile, int $code = 0, Throwable $previous = null)
    {
        parent::__construct('Sound file "' . $soundFile . '" doesn\'t exist.', $code, $previous);
    }
}
