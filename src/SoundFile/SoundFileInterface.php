<?php

/**
 * CLI Notification.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\CLINotification\SoundFile;

interface SoundFileInterface
{
    /**
     * Returns the full path to an audio file, that can be played.
     *
     * @return string
     */
    public function getValue(): string;
}
