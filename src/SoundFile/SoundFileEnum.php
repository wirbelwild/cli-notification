<?php

/**
 * CLI Notification.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\CLINotification\SoundFile;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 * @method static SoundFileEnum APPOINTED()
 * @method static SoundFileEnum CASE_CLOSED()
 * @method static SoundFileEnum COINS()
 * @method static SoundFileEnum JOB_DONE()
 * @method static SoundFileEnum LIGHT()
 * @method static SoundFileEnum OPEN_UP()
 * @method static SoundFileEnum WHEN()
 * @method static SoundFileEnum YOU_WOULDNT_BELIEVE()
 */
final class SoundFileEnum extends Enum implements SoundFileInterface
{
    private const APPOINTED = 'appointed.mp3';
    private const CASE_CLOSED = 'case-closed.mp3';
    private const COINS = 'coins.mp3';
    private const JOB_DONE = 'job-done.mp3';
    private const LIGHT = 'light.mp3';
    private const OPEN_UP = 'open-up.mp3';
    private const WHEN = 'when.mp3';
    private const YOU_WOULDNT_BELIEVE = 'you-wouldnt-believe.mp3';

    public function getValue(): string
    {
        return dirname(__FILE__, 3) . DIRECTORY_SEPARATOR .
            'sounds' . DIRECTORY_SEPARATOR . $this->value
        ;
    }
}
