<?php

/**
 * CLI Notification.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\CLINotification\PHPStan;

use BitAndBlack\CLINotification\SoundFile\SoundFileEnum;
use PHPStan\Reflection\ConstantReflection;
use PHPStan\Rules\Constants\AlwaysUsedClassConstantsExtension;

class ConstantsExtension implements AlwaysUsedClassConstantsExtension
{
    /** @var array<int, string>  */
    private array $knownConstants = [];

    public function __construct()
    {
        array_push($this->knownConstants, ...SoundFileEnum::keys());
    }

    public function isAlwaysUsed(ConstantReflection $constant): bool
    {
        return in_array($constant->getName(), $this->knownConstants, true);
    }
}
