<?php

/**
 * CLI Notification.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\CLINotification;

use Joli\JoliNotif\Notification;
use Joli\JoliNotif\NotifierFactory;

class Popup
{
    /**
     * @param string $title
     * @param string $message
     * @param string|null $icon
     * @return bool
     */
    public static function show(string $title, string $message, string $icon = null): bool
    {
        $notifier = NotifierFactory::create();

        $notification = new Notification();
        $notification
            ->setTitle($title)
            ->setBody($message)
        ;
        
        if (null !== $icon && file_exists($icon)) {
            $notification->setIcon($icon);
        }

        return $notifier->send($notification);
    }
}
