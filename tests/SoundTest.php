<?php

/**
 * CLI Notification.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\CLINotification\Tests;

use BitAndBlack\CLINotification\Exception\SoundFileMissingException;
use BitAndBlack\CLINotification\Sound;
use BitAndBlack\CLINotification\SoundFile\SoundFileEnum;
use BitAndBlack\CLINotification\SoundFile\SoundFileInterface;
use PHPUnit\Framework\TestCase;

class SoundTest extends TestCase
{
    /**
     * @throws SoundFileMissingException
     */
    public function testPlaysSoundOneAfterAnother(): void
    {
        $timeStart = microtime(true);

        Sound::play(SoundFileEnum::APPOINTED());
        Sound::play(SoundFileEnum::APPOINTED());

        $time = microtime(true) - $timeStart;

        self::assertGreaterThan(
            3,
            $time
        );
    }

    public function testThrowsExceptionOnMissingFile(): void
    {
        $this->expectException(SoundFileMissingException::class);

        $soundFileProvider = new class() implements SoundFileInterface {
            public function getValue(): string
            {
                return 'MISSING';
            }
        };

        Sound::play($soundFileProvider);
    }
}
