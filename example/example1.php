<?php

use BitAndBlack\CLINotification\Sound;
use BitAndBlack\CLINotification\SoundFile\SoundFileEnum;

require '../vendor/autoload.php';

echo 'Now playing sound... ';

$success = Sound::play(
    SoundFileEnum::JOB_DONE()
);

echo 'Done ' . ($success ? 'with' : 'without') . ' success.' . PHP_EOL;
