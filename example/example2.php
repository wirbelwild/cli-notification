<?php

use BitAndBlack\CLINotification\Popup;

require '../vendor/autoload.php';

echo 'Now showing popup... ';

$success = Popup::show(
    'Hello world!',
    'This is great, isn\'t it?'
);

echo 'Done ' . ($success ? 'with' : 'without') . ' success.' . PHP_EOL;
